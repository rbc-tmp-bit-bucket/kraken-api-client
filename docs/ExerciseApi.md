# kraken_api_client.ExerciseApi

All URIs are relative to *https://api.krakenflex.systems/interview-tests-mock-api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**outages_get**](ExerciseApi.md#outages_get) | **GET** /outages | Returns all outages in our system.
[**site_info_site_id_get**](ExerciseApi.md#site_info_site_id_get) | **GET** /site-info/{siteId} | Returns information about a specific site.
[**site_outages_site_id_post**](ExerciseApi.md#site_outages_site_id_post) | **POST** /site-outages/{siteId} | Posts outages for a specific site with enhanced information.


# **outages_get**
> Outages outages_get()

Returns all outages in our system.

An outage is when a device can no longer provide service and is declared as offline. Each outage consists of a device ID, begin time, and end time.

### Example

* Api Key Authentication (api_key):

```python
import time
import kraken_api_client
from kraken_api_client.api import exercise_api
from kraken_api_client.model.error_message import ErrorMessage
from kraken_api_client.model.outages import Outages
from pprint import pprint
# Defining the host is optional and defaults to https://api.krakenflex.systems/interview-tests-mock-api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = kraken_api_client.Configuration(
    host = "https://api.krakenflex.systems/interview-tests-mock-api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: api_key
configuration.api_key['api_key'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['api_key'] = 'Bearer'

# Enter a context with an instance of the API client
with kraken_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = exercise_api.ExerciseApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Returns all outages in our system.
        api_response = api_instance.outages_get()
        pprint(api_response)
    except kraken_api_client.ApiException as e:
        print("Exception when calling ExerciseApi->outages_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Outages**](Outages.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Outages response. |  -  |
**403** | You do not have the required permissions to make this request. |  -  |
**429** | You&#39;ve exceeded your limit for your API key. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **site_info_site_id_get**
> SiteInfo site_info_site_id_get(site_id)

Returns information about a specific site.

The site information contains the ID and name of the site. It also contains a list of devices that make up the site.

### Example

* Api Key Authentication (api_key):

```python
import time
import kraken_api_client
from kraken_api_client.api import exercise_api
from kraken_api_client.model.error_message import ErrorMessage
from kraken_api_client.model.site_info import SiteInfo
from pprint import pprint
# Defining the host is optional and defaults to https://api.krakenflex.systems/interview-tests-mock-api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = kraken_api_client.Configuration(
    host = "https://api.krakenflex.systems/interview-tests-mock-api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: api_key
configuration.api_key['api_key'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['api_key'] = 'Bearer'

# Enter a context with an instance of the API client
with kraken_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = exercise_api.ExerciseApi(api_client)
    site_id = "pear-tree" # str | Identifier for a site

    # example passing only required values which don't have defaults set
    try:
        # Returns information about a specific site.
        api_response = api_instance.site_info_site_id_get(site_id)
        pprint(api_response)
    except kraken_api_client.ApiException as e:
        print("Exception when calling ExerciseApi->site_info_site_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **site_id** | **str**| Identifier for a site |

### Return type

[**SiteInfo**](SiteInfo.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Site information response. |  -  |
**403** | You do not have the required permissions to make this request. |  -  |
**404** | You have requested a resource that does not exist. |  -  |
**429** | You&#39;ve exceeded your limit for your API key. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **site_outages_site_id_post**
> site_outages_site_id_post(site_id)

Posts outages for a specific site with enhanced information.

The outages posted should contain a device ID, device name, begin time, and end time.

### Example

* Api Key Authentication (api_key):

```python
import time
import kraken_api_client
from kraken_api_client.api import exercise_api
from kraken_api_client.model.error_message import ErrorMessage
from kraken_api_client.model.enhanced_outages import EnhancedOutages
from pprint import pprint
# Defining the host is optional and defaults to https://api.krakenflex.systems/interview-tests-mock-api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = kraken_api_client.Configuration(
    host = "https://api.krakenflex.systems/interview-tests-mock-api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: api_key
configuration.api_key['api_key'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['api_key'] = 'Bearer'

# Enter a context with an instance of the API client
with kraken_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = exercise_api.ExerciseApi(api_client)
    site_id = "pear-tree" # str | Identifier for a site
    enhanced_outages = EnhancedOutages([
        {
            id="44c02564-a229-4f51-8ded-cc7bcb202566",
            name="Partridge",
            begin=dateutil_parser('2022-01-01T00:00:00Z'),
            end=dateutil_parser('2022-01-02T12:01:59.123Z'),
        },
    ]) # EnhancedOutages |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Posts outages for a specific site with enhanced information.
        api_instance.site_outages_site_id_post(site_id)
    except kraken_api_client.ApiException as e:
        print("Exception when calling ExerciseApi->site_outages_site_id_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Posts outages for a specific site with enhanced information.
        api_instance.site_outages_site_id_post(site_id, enhanced_outages=enhanced_outages)
    except kraken_api_client.ApiException as e:
        print("Exception when calling ExerciseApi->site_outages_site_id_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **site_id** | **str**| Identifier for a site |
 **enhanced_outages** | [**EnhancedOutages**](EnhancedOutages.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfully processed request. |  -  |
**403** | You do not have the required permissions to make this request. |  -  |
**404** | You have requested a resource that does not exist. |  -  |
**429** | You&#39;ve exceeded your limit for your API key. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

