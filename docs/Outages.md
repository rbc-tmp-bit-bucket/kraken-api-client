# Outages

An array outages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** | An array outages | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


