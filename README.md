# kraken-api-client
No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

## Requirements.

Python >=3.6

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import kraken_api_client
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import kraken_api_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python

import time
import kraken_api_client
from pprint import pprint
from kraken_api_client.api import exercise_api
from kraken_api_client.model.enhanced_outages import EnhancedOutages
from kraken_api_client.model.error_message import ErrorMessage
from kraken_api_client.model.outages import Outages
from kraken_api_client.model.site_info import SiteInfo
# Defining the host is optional and defaults to https://api.krakenflex.systems/interview-tests-mock-api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = kraken_api_client.Configuration(
    host = "https://api.krakenflex.systems/interview-tests-mock-api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: api_key
configuration.api_key['api_key'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['api_key'] = 'Bearer'


# Enter a context with an instance of the API client
with kraken_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = exercise_api.ExerciseApi(api_client)
    
    try:
        # Returns all outages in our system.
        api_response = api_instance.outages_get()
        pprint(api_response)
    except kraken_api_client.ApiException as e:
        print("Exception when calling ExerciseApi->outages_get: %s\n" % e)
```

## Documentation for API Endpoints

All URIs are relative to *https://api.krakenflex.systems/interview-tests-mock-api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ExerciseApi* | [**outages_get**](docs/ExerciseApi.md#outages_get) | **GET** /outages | Returns all outages in our system.
*ExerciseApi* | [**site_info_site_id_get**](docs/ExerciseApi.md#site_info_site_id_get) | **GET** /site-info/{siteId} | Returns information about a specific site.
*ExerciseApi* | [**site_outages_site_id_post**](docs/ExerciseApi.md#site_outages_site_id_post) | **POST** /site-outages/{siteId} | Posts outages for a specific site with enhanced information.


## Documentation For Models

 - [EnhancedOutages](docs/EnhancedOutages.md)
 - [ErrorMessage](docs/ErrorMessage.md)
 - [Outages](docs/Outages.md)
 - [SiteInfo](docs/SiteInfo.md)
 - [SiteInfoDevices](docs/SiteInfoDevices.md)


## Documentation For Authorization


## api_key

- **Type**: API key
- **API key parameter name**: x-api-key
- **Location**: HTTP header


## Author




## Notes for Large OpenAPI documents
If the OpenAPI document is large, imports in kraken_api_client.apis and kraken_api_client.models may fail with a
RecursionError indicating the maximum recursion limit has been exceeded. In that case, there are a couple of solutions:

Solution 1:
Use specific imports for apis and models like:
- `from kraken_api_client.api.default_api import DefaultApi`
- `from kraken_api_client.model.pet import Pet`

Solution 2:
Before importing the package, adjust the maximum recursion limit as shown below:
```
import sys
sys.setrecursionlimit(1500)
import kraken_api_client
from kraken_api_client.apis import *
from kraken_api_client.models import *
```

